// Package server provides the functionality of parsing
// of AIS140, GTPL raw data into a AIS140 device model.
//
// It consists of utilities which insert the parsed data into
// the live and history databases in Mongo.
//
// TCP server along with a port is initialized in StartServer
package server
