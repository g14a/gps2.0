package main

import (
	"gitlab.com/gpsv2/server"
)

// main starts the server
func main() {
	server.StartServer()
}
